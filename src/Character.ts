export interface Character { 
    id?: number; 
    name: string; 
    height: number;
    weight: number; 
    image?: string; 
}